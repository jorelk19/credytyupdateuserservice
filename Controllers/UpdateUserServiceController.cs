﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestCredytyDB.Business;
using TestCredytyDB.Entities;

namespace TestCredytyDB.Controllers
{
    [Route("api/UpdateUserService")]
    [ApiController]
    public class UpdateUserServiceController : ControllerBase
    {
        /// <summary>
        /// Method used to update a user
        /// </summary>
        /// <param name="user">User data</param>
        /// <returns>User result</returns>
        [HttpPut]
        [Route("Update")]
        public UserResponse UpdateUser(User user)
        {
            return UpdateUserServiceManager.Instance.UpdateUser(user);
        }
    }
}
