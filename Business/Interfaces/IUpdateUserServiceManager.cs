﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCredytyDB.Entities;

namespace TestCredytyDB.Business.Interfaces
{
    interface IUpdateUserServiceManager
    {
        /// <summary>
        /// Method used to update the user data by specific email
        /// </summary>
        /// <param name="user">User to update</param>
        /// <returns>User data</returns>
        UserResponse UpdateUser(User user);
    }
}
