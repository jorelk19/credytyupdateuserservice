﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCredytyDB.Business.Interfaces;
using TestCredytyDB.Entities;
using TestCredytyDB.Respository;

namespace TestCredytyDB.Business
{
    public class UpdateUserServiceManager : IUpdateUserServiceManager
    {
        /// <summary>
        /// Private var to get the instance to manage the singleton
        /// </summary>
        private static UpdateUserServiceManager instance;

        /// <summary>
        /// Instance to manage the singleton for the class
        /// </summary>
        public static UpdateUserServiceManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UpdateUserServiceManager();
                }                
                return instance;
            }
        }

        /// <summary>
        /// Method to get a specific user
        /// </summary>
        /// <param name="email">Mail to search user data</param>
        /// <returns>Return user data</returns>
        public UserResponse UpdateUser(User user)
        {
            try
            {
                return okResponse(UserRepository.Instance.UpdateUser(user));
            }
            catch (Exception ex)
            {
                return badResponse(ex);
            }
        }

        /// <summary>
        /// Method used to resolve the success response in methods
        /// </summary>
        /// <param name="data">Data to return</param>
        /// <returns>User response object</returns>
        private UserResponse okResponse(object data)
        {
            return new UserResponse
            {
                Code = 200,
                Data = JsonConvert.SerializeObject(data),
                ServerError = string.Empty
            };
        }

        /// <summary>
        /// Method used to resolve the bad response in methods
        /// </summary>
        /// <param name="exception">Exception throwed by the method</param>
        /// <returns>User response object</returns>
        private UserResponse badResponse(Exception exception)
        {
            return new UserResponse
            {
                Code = 400,
                Data = null,
                ServerError = exception.Message
            };
        }

    }
}
