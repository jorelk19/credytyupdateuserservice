﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCredytyDB.Entities
{
    public class UserResponse
    {
        public string ServerError { get; set; }
        public int Code { get; set; }
        public string Data { get; set; }
    }
}
