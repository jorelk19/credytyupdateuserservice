﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCredytyDB.Entities;

namespace TestCredytyDB.UnitTest.Mocks
{
    public class MockDataService
    {
        public UserResponse MockUserResponse()
        {
            return new UserResponse
            {
                Code = 200,
                Data = JsonConvert.SerializeObject(MockUser()),
                ServerError = string.Empty
            };           
        }

        public UserResponse MockUserResponseList()
        {
            return new UserResponse
            {
                Code = 200,
                Data = JsonConvert.SerializeObject(MockUserList()),
                ServerError = string.Empty
            };
        }

        public UserResponse MockUserResponseBool()
        {
            return new UserResponse
            {
                Code = 200,
                Data = JsonConvert.SerializeObject(true),
                ServerError = string.Empty
            };
        }

        public List<User> MockUserList()
        {
            var userList = new List<User>();
            userList.Add(new User { UserId = 1, Email = "mockemail@mock.com", Name = "Mock Name", Password = "Mockpass" });
            userList.Add(new User { UserId = 2, Email = "mockemail1@mock.com", Name = "Mock Name1", Password = "Mockpass1" });
            return userList;
        }

        public User MockUser()
        {
            return new User { UserId = 1, Email = "mockemail@mock.com", Name = "Mock Name", Password = "Mockpass" };
        }


    }
}
