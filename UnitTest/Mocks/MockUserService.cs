﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCredytyDB.Business.Interfaces;
using TestCredytyDB.Entities;

namespace TestCredytyDB.UnitTest.Mocks
{
    public class MockUserService : IUpdateUserServiceManager
    {
        private MockDataService mockDataService = new MockDataService();
        public UserResponse CreateUser(User user)
        {
            return mockDataService.MockUserResponseBool();
        }

        public UserResponse DeleteUser(int userId)
        {
            return mockDataService.MockUserResponseBool();
        }

        public UserResponse GetAllUsers()
        {
            return mockDataService.MockUserResponseList();
        }

        public UserResponse GetUser(int userId)
        {
            return mockDataService.MockUserResponse();            
        }

        public UserResponse UpdateUser(User user)
        {
            return mockDataService.MockUserResponseBool();
        }
    }
}
