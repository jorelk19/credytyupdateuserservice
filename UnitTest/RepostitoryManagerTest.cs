﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NUnit.Framework;
using TestCredytyDB.Business.Interfaces;
using TestCredytyDB.Controllers;
using TestCredytyDB.Respository;
using TestCredytyDB.UnitTest.Mocks;

namespace TestCredytyDB.UnitTest
{
    [TestFixture]
    public class RepostitoryManagerTest
    {
        MockDataService mockDataManager;
        MockUserService mockUserService;

        [SetUp]
        public void SetUp()
        {
            mockDataManager  = new MockDataService();
            mockUserService = new MockUserService();
        }

        [TestMethod()]
        public void createUserTest()
        {
            //WHEN
            var mockServiceManager = new Mock<IUpdateUserServiceManager>();
            mockServiceManager.Setup(m => m.UpdateUser(mockDataManager.MockUser())).Returns(mockDataManager.MockUserResponse);

            var userService = new UpdateUserServiceController();
            var result = userService.UpdateUser(mockDataManager.MockUser());

            NUnit.Framework.Assert.IsTrue(result.Data != null);
        }

        [TestMethod()]
        public void openConnectionTest()
        {
          
        }

        
    }
}
